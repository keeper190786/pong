import { transferVector, Vector } from "./Vector";

describe("Vector", () => {
  it("should invert curDirection", () => {
    const a = new Vector(0, -1);
    const b = new Vector(0, 1);
    transferVector(a, b, new Vector(1, 0));
    expect(a).toEqual(new Vector(1, -1));
    expect(b).toEqual(new Vector(-1, 1));
  });
});
