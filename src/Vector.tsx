export class Vector {
  constructor(public x = 0, public y = 0) {}

  getTuple(): [number, number] {
    return [this.x, this.y];
  }

  scale(factor: number) {
    return new Vector(this.x * factor, this.y * factor);
  }

  add(a: Vector) {
    return new Vector(a.x + this.x, a.y + this.y);
  }

  subtract(a: Vector) {
    return new Vector(this.x - a.x, this.y - a.y);
  }

  quad() {
    return this.x ** 2 + this.y ** 2;
  }

  scalarMultiply(a: Vector) {
    return this.x * a.x + this.y * a.y;
  }

  normed(): Vector {
    const norm = 1 / Math.sqrt(this.quad());
    return new Vector(norm * this.x, norm * this.y);
  }

  normedPlusNorm(): [Vector, number] {
    const norm = Math.sqrt(this.quad());
    return [new Vector(this.x / norm, this.y / norm), norm];
  }

  toString() {
    return this.x + " " + this.y;
  }
}

export function transferVector(
  source: Vector,
  target: Vector,
  transfer: Vector
) {
  source.x += transfer.x;
  source.y += transfer.y;
  target.x -= transfer.x;
  target.y -= transfer.y;
}

export function getUnitVector(): Vector {
  const rng = Math.random() * 2 * Math.PI;
  return new Vector(Math.cos(rng), Math.sin(rng));
}

export function inCircle(
  test: Vector,
  circleCenter: Vector,
  circleRadius: number
): boolean {
  return test.subtract(circleCenter).quad() < circleRadius * circleRadius;
}

export function reflect(curDirection: Vector, normal: Vector) {
  return curDirection.add(
    normal.scale((-2 * normal.scalarMultiply(curDirection)) / normal.quad())
  );
}
