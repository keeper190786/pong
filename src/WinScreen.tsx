import React from "react";

export interface ScreenInfo {
  renderDiv: () => JSX.Element;
}

export function WinScreen({
  timeLeftInMs,
  content,
}: {
  timeLeftInMs: number;
  content: JSX.Element;
}) {
  if (timeLeftInMs > 0) {
    return (
      <div className={"win-screen"}>
        {" "}
        {content}{" "}
        <div
          style={{ position: "absolute", bottom: "10px", marginLeft: "auto" }}
        >
          {" "}
          Please wait: {(timeLeftInMs / 1000 + 0.5).toFixed(0)}s{" "}
        </div>{" "}
      </div>
    );
  } else {
    return null;
  }
}
