import * as React from "react";
import { Ball, position } from "./model";

interface BackgroundCanvasProps {
  // The color of the parabola
  parabolaColor: string;
  // The phase of the parabola
  balls: Ball[];
  gravity: number;
}

export const BackgroundCanvas: React.FC<BackgroundCanvasProps> = ({
  parabolaColor,
  balls,
  gravity,
}) => {
  const canvasRef = React.useRef<HTMLCanvasElement>(null);

  React.useEffect(() => {
    // Get the canvas element from the ref
    const canvas = canvasRef.current;
    if (canvas === null) {
      return;
    }
    const ww = window.innerWidth;
    // Set the canvas to span the whole window
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;

    // Get the canvas context
    const ctx = canvas.getContext("2d");
    if (ctx === null) {
      console.error("Could not get canvas context");
      return;
    }

    const samples = 10;
    const max_time = 100;
    for (const ball of balls) {
      if (!ball.laser) continue;

      // Set the stroke style
      ctx.strokeStyle = parabolaColor;
      const lines = [];
      for (let i = 0; i < samples; i++) {
        const point = position(ball, (i * max_time) / samples, gravity);

        lines.push({
          x: ww - point.x,
          y: point.y,
          color: `rgba(187, 43, 83, ${(samples - Math.abs(i)) / samples})`,
        });
      }

      var linewidth = 2;

      ctx.lineCap = "round";
      ctx.lineJoin = "round";

      for (var i = 1; i < lines.length; i++) {
        // calculate the smaller part of the line segment over
        //     which the gradient will run
        let p0 = lines[i - 1];
        let p1 = lines[i];
        let dx = p1.x - p0.x;
        let dy = p1.y - p0.y;
        let angle = Math.atan2(dy, dx);
        let p0x = p0.x + linewidth * Math.cos(angle);
        let p0y = p0.y + linewidth * Math.sin(angle);
        let p1x = p1.x + linewidth * Math.cos(angle + Math.PI);
        let p1y = p1.y + linewidth * Math.sin(angle + Math.PI);
        // determine where the gradient starts and ends
        let g: CanvasGradient;
        if (i === 1) {
          g = ctx.createLinearGradient(p0.x, p0.y, p1x, p1y);
        } else if (i === lines.length - 1) {
          g = ctx.createLinearGradient(p0x, p0y, p1.x, p1.y);
        } else {
          g = ctx.createLinearGradient(p0x, p0y, p1x, p1y);
        }

        // add the gradient color stops
        // and draw the gradient line from p0 to p1
        g.addColorStop(0, p0.color);
        g.addColorStop(1, p1.color);
        ctx.beginPath();
        ctx.moveTo(p0.x, p0.y);
        ctx.lineTo(p1.x, p1.y);
        ctx.strokeStyle = g;
        ctx.lineWidth = linewidth;
        ctx.stroke();
      }
    }

    // eslint-disable-next-line
  }, [balls]);

  return (
    <canvas
      ref={canvasRef}
      style={{
        position: "fixed",
        top: 0,
        left: 0,
        zIndex: -10,
      }}
      width={window.innerWidth}
      height={window.innerHeight}
    />
  );
};
