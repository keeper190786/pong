import { Vector } from "./Vector";
import { PaddleInfo } from "./Paddle";

export enum StateActionKind {
  STEP = "step",
  NEW_PADDLE_STATE_LEFT = "newPaddleStateLeft",
  NEW_PADDLE_STATE_RIGHT = "newPaddleStateRight",
  AI_LEFT = "toggleAiLeft",
  AI_LEFT_ON = "aiLeftOn",
  AI_LEFT_OFF = "aiLeftOff",
  AI_RIGHT = "toggleAiRight",
  SET_GRAVITY = "setGravity",
}

export type StateActionPaddle = {
  type:
    | StateActionKind.NEW_PADDLE_STATE_LEFT
    | StateActionKind.NEW_PADDLE_STATE_RIGHT;
  payload: { vy?: number; target?: number; centerX?: number; height?: number };
};
export type StateActionGravity = {
  type: StateActionKind.SET_GRAVITY;
  payload: number;
};

export type StateActionMisc = {
  type:
    | StateActionKind.STEP
    | StateActionKind.AI_RIGHT
    | StateActionKind.AI_LEFT
    | StateActionKind.AI_LEFT_OFF
    | StateActionKind.AI_LEFT_ON;
};
export type StateAction =
  | StateActionMisc
  | StateActionGravity
  | StateActionPaddle;

export interface TimedPopUp {
  timer: number;
  content: JSX.Element;
}

export type PowerUpType =
  | "increaseGravity"
  | "decreaseGravity"
  | "laser"
  | "addBall"
  | "shrink"
  | "grow";

export interface PowerUpBase extends Phase {
  type: PowerUpType;
  cooldown: number;

  applyEffectOn(ball: Ball, state: State): void;

  image: string;
  primaryColor: string;
  secundaryColor: string;
  shadowColor: [number, number, number];
}

export interface Phase {
  position: Vector;
  velocityDirection: Vector;
  velocityMagnitude: number;
}

export const velocity = (phase: Phase) =>
  phase.velocityDirection.scale(phase.velocityMagnitude);
export const position = (
  phase: Phase,
  timeInFrames: number,
  gravity: number
) => {
  if (timeInFrames === 0) return phase.position;
  const dueVelocity = phase.velocityDirection.scale(
    phase.velocityMagnitude * timeInFrames
  );
  if (gravity === 0) return phase.position.add(dueVelocity);
  const dueGravityAndVelocity = new Vector(
    dueVelocity.x,
    dueVelocity.y + ((0.5 * gravity) / 2000) * timeInFrames * timeInFrames
  );
  return phase.position.add(dueGravityAndVelocity);
};
export type Ball = Phase & { laser: boolean };

export interface State {
  balls: Ball[];
  powerUps: PowerUpBase[];
  left: PaddleInfo;
  right: PaddleInfo;
  speed: number;
  ai: [boolean, boolean];
  gravity: number;
  laser: boolean;
  timedPopUp: TimedPopUp;
}
