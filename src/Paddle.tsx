import React from "react";
import { Vector } from "./Vector";
import { ShinyCircle } from "./Circle";
import { paddleWidth } from "./Engine";

type Side = "left" | "right";
export interface PaddleInfo {
  side: Side;
  top: number;
  vy: number;
  score: number;
  target: number;
  centerX: number;
  paddleHeight: number;
  paddleDiameter: number; // must be greater than paddleHeight
}

export function center(paddle: PaddleInfo) {
  return new Vector(paddle.centerX, paddle.top + paddle.paddleHeight * 0.5);
}

export const centerFromPaddle = (paddle: PaddleInfo) =>
  Math.sqrt(
    0.25 * paddle.paddleDiameter ** 2 - 0.25 * paddle.paddleHeight ** 2
  );

export function paddleCircleCenter(paddle: PaddleInfo): number {
  return paddle.side === "right"
    ? -centerFromPaddle(paddle) + paddleWidth
    : window.innerWidth + centerFromPaddle(paddle) - paddleWidth;
}

function paddleRight(paddle: PaddleInfo): number {
  if (paddle.side === "left") return window.innerWidth - paddleWidth;
  if (paddle.side === "right") return 0;
  throw new Error("no paddle to give right");
}

function paddleColor(paddle: PaddleInfo): string {
  if (paddle.side === "left") return "red";
  if (paddle.side === "right") return "green";
  throw new Error("no paddle to give color");
}

export function Paddle(paddle: PaddleInfo) {
  return (
    <>
      <ShinyCircle
        center={center(paddle)}
        radius={0.5 * paddle.paddleDiameter}
        color={"white"}
        color2={paddleColor(paddle)}
        color3={"black"}
        relativeCenterReflectionInPercent={new Vector(0, 0)}
        zIndex={"0"}
        src={""}
      />
      <div
        style={{
          position: "fixed",
          top: paddle.top,
          right: paddleRight(paddle),
          width: paddleWidth + "px",
          height: paddle.paddleHeight + "px",
          backgroundColor: "black",
        }}
      ></div>
    </>
  );
}
