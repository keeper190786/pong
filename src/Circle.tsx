import { Vector } from "./Vector";
interface CircleParameter {
  center: Vector;
  radius: number;
  color: string;
  zIndex: string;
} //TODO: color better

interface ShinyCircleParameter extends CircleParameter {
  color2: string;
  color3: string;
  relativeCenterReflectionInPercent: Vector;
  src: string;
}

export function SphericalShadow({
  center,
  radius,
  color,
}: {
  center: Vector;
  radius: number;
  color: [number, number, number];
}) {
  const top = center.y - radius;
  const right = center.x - radius;
  return (
    <div
      style={{
        display: "block",
        position: "fixed",
        top,
        right,
        borderRadius: radius,
        zIndex: -1,
        width: 2 * radius + "px",
        height: 2 * radius + "px",
        background: `radial-gradient(circle at 50% 50%, rgba(${color[0]}, ${color[1]}, ${color[2]}, 1) 0%, rgba(0, 0, 0, 0) 75%)`,
        opacity: 1.0,
      }}
    ></div>
  );
}

export function ShinyCircle({
  center,
  radius,
  color,
  color2,
  color3,
  relativeCenterReflectionInPercent,
  zIndex = "auto",
}: ShinyCircleParameter) {
  const top = center.y - radius;
  const right = center.x - radius;

  return (
    <div
      className={"shiny-circle"}
      style={{
        top,
        right,
        borderRadius: radius,
        zIndex,
        width: 2 * radius + "px",
        height: 2 * radius + "px",
        background: `radial-gradient(circle at ${
          relativeCenterReflectionInPercent.x + 50
        }% ${
          relativeCenterReflectionInPercent.y + 50
        }%, lightgrey 1px, ${color} 3%, ${color2} 60%, ${color3} 90%, ${color3} 100% )`,
      }}
    ></div>
  );
}

export function NotsoShinyCircle({
  center,
  radius,
  color,
  color2,
  color3,
  relativeCenterReflectionInPercent,
  zIndex = "auto",
}: ShinyCircleParameter) {
  const top = center.y - radius;
  const right = center.x - radius;

  return (
    <div
      className={"shiny-circle"}
      style={{
        top,
        right,
        borderRadius: radius,
        zIndex,
        width: 2 * radius + "px",
        height: 2 * radius + "px",
        background: `radial-gradient(circle at ${
          relativeCenterReflectionInPercent.x + 50
        }% ${
          relativeCenterReflectionInPercent.y + 50
        }%, white 1px, ${color} 3%, ${color2} 75%, ${color3} 90%, ${color3} 100% )`,
      }}
    ></div>
  );
}

export function NotsoShinyCircleWithImage({
  center,
  radius,
  color,
  color2,
  color3,
  relativeCenterReflectionInPercent,
  zIndex = "auto",
  src,
}: ShinyCircleParameter) {
  const top = center.y - radius;
  const right = center.x - radius;

  return (
    <div
      className={"shiny-circle"}
      style={{
        top,
        right,
        borderRadius: radius,
        zIndex,
        width: 2 * radius + "px",
        height: 2 * radius + "px",
        background: `radial-gradient(circle at ${
          relativeCenterReflectionInPercent.x + 50
        }% ${
          relativeCenterReflectionInPercent.y + 50
        }%, white 1px, ${color} 3%, ${color2} 75%, ${color3} 90%, ${color3} 100% )`,
        opacity: 1,
      }}
    >
      <img className={"power-ups"} src={src} alt={"PowerUp"} />
    </div>
  );
}
