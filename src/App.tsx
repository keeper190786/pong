import React from "react";
import "./App.css";
import { Pong } from "./Pong";

function App() {
  return <Pong />;
}

export default App;
