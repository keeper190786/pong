import settingsIcon from "./settings.png";
import { State, StateAction, StateActionKind } from "./model";

//https://zillow.github.io/react-slider/
interface StateParameter {
  state: State;
  reduceState: (action: StateAction) => void;
}

export function Settings({ state, reduceState }: StateParameter) {
  return (
    <div className="dropdown" id={"settings"}>
      <span>
        <img id={"settings-icon"} src={settingsIcon} alt={"Settings"} />
      </span>
      <div className="dropdown-content">
        <label>
          {"Toggle left AI"}
          <input
            type="checkbox"
            name={"AIRight"}
            checked={state.ai[1]}
            onChange={() => {
              reduceState({
                type: StateActionKind.AI_RIGHT,
              });
            }}
            className="form-check-input"
          />
        </label>
      </div>
    </div>
  );
}
