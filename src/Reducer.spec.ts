import { reflect, Vector } from "./Vector";
import { foldBackToDomain } from "./Engine";

describe("Reducer", () => {
  it("should invert curDirection", () => {
    const result = reflect(new Vector(0, -1), new Vector(0, 1));
    expect(result).toEqual(new Vector(0, 1));
  });

  it("should reflect curDirection", () => {
    const result = reflect(new Vector(-1, -1), new Vector(0, 1));
    expect(result).toEqual(new Vector(-1, 1));
  });

  it("should fold back to Domain", () => {
    expect(foldBackToDomain(-1.1, 1)).toBeCloseTo(0.9, 8);
    expect(foldBackToDomain(-0.3, 1)).toBeCloseTo(0.3, 8);
    expect(foldBackToDomain(1.5, 1)).toBeCloseTo(0.5, 8);
    expect(foldBackToDomain(1.3, 1)).toBeCloseTo(0.7, 8);
    expect(foldBackToDomain(2.3, 1)).toBeCloseTo(0.3, 8);
  });
});
