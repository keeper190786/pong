import { Ball, renderPowerUp } from "./Ball";
import { Paddle } from "./Paddle";
import { Reducer, useEffect, useReducer, useRef } from "react";
import { reducer } from "./Reducer";
import { WinScreen } from "./WinScreen";
import { ballRadius, createInitialState, fps } from "./Engine";
import { BackgroundCanvas } from "./parabola";
import _ from "lodash";
import { State, StateAction, StateActionKind } from "./model";

export function Pong() {
  const [state, reduceState] = useReducer<Reducer<State, StateAction>>(
    reducer,
    createInitialState(),
    undefined
  );

  const setLeftTarget = (target: number) => {
    reduceState({
      type: StateActionKind.NEW_PADDLE_STATE_LEFT,
      payload: { vy: state.left.vy, target: target },
    });
  };

  useEffect(() => {
    setInterval(() => {
      reduceState({ type: StateActionKind.STEP });
    }, 1000 / fps);

    function handleResize() {
      reduceState({
        type: StateActionKind.NEW_PADDLE_STATE_LEFT,
        payload: {},
      });
    }

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const lastMoveTime = useRef(0);
  const positionX = useRef(90);
  useEffect(() => {
    document.addEventListener(
      "touchmove",
      _.throttle((event: TouchEvent) => {
        const touch = event.touches[0];

        setLeftTarget(touch.clientY);
        positionX.current = (touch.clientX / window.innerWidth) * 100;
        lastMoveTime.current = new Date().getTime();
      }, 1000 / fps),
      { passive: false }
    );

    document.addEventListener(
      "mousemove",
      _.throttle((event: MouseEvent) => {
        setLeftTarget(event.clientY);
        positionX.current = (event.clientX / window.innerWidth) * 100;
        lastMoveTime.current = new Date().getTime();
      }, 1000 / fps),
      { passive: false }
    );
    const interval = setInterval(() => {
      const currentTime = new Date().getTime();
      const sinceActive = currentTime - lastMoveTime.current;
      if (sinceActive > 3800) {
        // Trigger 'ai left' action
        reduceState({ type: StateActionKind.AI_LEFT_ON });
      } else {
        reduceState({ type: StateActionKind.AI_LEFT_OFF });
      }
    }, 10000 / fps);

    return () => {
      document.removeEventListener("touchmove", () => {});
      document.removeEventListener("mousemove", () => {});
      clearInterval(interval);
    };

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [state.ai[0]]);

  const ballComponents: JSX.Element[] = [];
  for (let index = 0; index < state.balls.length; index++) {
    const ball = state.balls[index];
    if (
      ball.position.x > -ballRadius &&
      ball.position.x < window.innerWidth + ballRadius
    ) {
      ballComponents.push(
        <Ball
          key={index}
          position={ball.position}
          velocityDirection={ball.velocityDirection}
          velocityMagnitude={ball.velocityMagnitude}
        />
      );
    }
  }

  const minorScoreRight = state.balls.filter(
    (ball) => ball.position.x > window.innerWidth
  ).length;
  const minorScoreLeft = state.balls.filter(
    (ball) => ball.position.x < 0
  ).length;

  //
  const powerUpComponents = state.powerUps
    .filter((powerUp) => {
      if (powerUp.type === "increaseGravity" && state.gravity === 100)
        return false;
      return !(powerUp.type === "decreaseGravity" && state.gravity === 0);
    })
    .map((powerUp) => renderPowerUp(powerUp));

  const movementHandle = () => {
    if (!state.ai[0]) {
      const sinceActive = new Date().getTime() - lastMoveTime.current;
      return (
        <div
          key={"moventHandle"}
          style={{
            top: `${state.left.target - 5}px`,
            left: 0,
            background: `radial-gradient(circle at 50% 50%,rgba(225, 30, 54, 1.0) ,rgba(225, 30, 54, 0.3) 20%, rgba(225, 30, 54, 0.0) 100%)`,
            backgroundPosition: `${
              ((positionX.current - 100) / 100) * window.innerWidth
            }px`,
            backgroundSize: "200% 100%",
            backgroundRepeat: "no-repeat",
            opacity: `${0.75 * (1 - sinceActive / 4000)}`,
            width: "100%",
            height: "10px",
            position: "fixed",
          }}
        />
      );
    }
  };

  return (
    <div>
      <BackgroundCanvas
        parabolaColor="red"
        balls={state.balls}
        gravity={state.gravity}
      />

      {powerUpComponents}
      {movementHandle()}

      <div id={"scoreboard"}>
        {`${minorScoreLeft} : (${
          state.balls.length - minorScoreLeft - minorScoreRight
        })  : ${minorScoreRight}`}
        <div className={"minor-scoreboard"}>
          {state.left.score} : {state.right.score}
        </div>
      </div>

      {ballComponents}

      <Paddle key={state.left.side} {...state.left} />
      <Paddle key={state.right.side} {...state.right} />

      <WinScreen
        timeLeftInMs={state.timedPopUp.timer}
        content={state.timedPopUp.content}
      />
    </div>
  );
}

export function credits() {
  return (
    <a href="https://www.flaticon.com/free-icons/gears" title="gears icons">
      Gears icons created by Triangle Squad - Flaticon
    </a>
  );
}
