import { Vector } from "./Vector";
import { NotsoShinyCircle, SphericalShadow } from "./Circle";
import { ballRadius, powerUpRadius } from "./Engine";
import { Phase, PowerUpBase } from "./model";

export function Ball({ position, velocityDirection }: Phase) {
  const color: [number, number, number] =
    velocityDirection.x > 0 ? [35, 0, 0] : [0, 35, 0];
  const { shadowPosition, shinyPosition } = calcEffectPosition(position);
  //alert(distanceShinyFactor+" "+shinyPositionInPerCentFromCenter)
  return (
    <>
      <SphericalShadow
        center={shadowPosition}
        radius={ballRadius * 1.05}
        color={color}
      />

      <NotsoShinyCircle
        center={position}
        radius={ballRadius}
        color={"lightgrey"}
        color2={"black"}
        color3={"grey"}
        relativeCenterReflectionInPercent={shinyPosition}
        zIndex={"auto"}
        src={""}
      />
    </>
  );
}

export function renderPowerUp(powerUp: PowerUpBase) {
  const { shadowPosition, shinyPosition } = calcEffectPosition(
    powerUp.position,
    powerUpRadius
  );

  const top = powerUp.position.y - powerUpRadius;
  const right = powerUp.position.x - powerUpRadius;
  const primaryColor =
    powerUp.cooldown === 0 ? powerUp.primaryColor : "lightgray";
  const secundaryColor = powerUp.cooldown === 0 ? powerUp.primaryColor : "gray";
  const shadowColor: [number, number, number] =
    powerUp.cooldown === 0 ? [0, 0, 0] : [180, 180, 180];
  return (
    <>
      <SphericalShadow
        center={shadowPosition}
        radius={powerUpRadius}
        color={shadowColor}
        //{`rgb(${powerUp.shadowColor[0]},${powerUp.shadowColor[1]},${powerUp.shadowColor[2]}`}
      />

      <div
        className={"shiny-circle"}
        style={{
          top,
          right,
          borderRadius: powerUpRadius,
          zIndex: -1,
          width: 2 * powerUpRadius + "px",
          height: 2 * powerUpRadius + "px",
          background: `radial-gradient(circle at ${shinyPosition.x + 50}% ${
            shinyPosition.y + 50
          }%, goldenrod 1px, goldenrod 3%, ${primaryColor} 75%, ${secundaryColor} 90%, ${
            powerUp.secundaryColor
          } 100% )`,
          opacity: powerUp.cooldown === 0 ? 1 : 0.2,
        }}
      >
        {powerUp.cooldown === 0 ? (
          <img className={"power-ups"} src={powerUp.image} alt={"PowerUp"} />
        ) : null}
      </div>
    </>
  );
}

const lightsource = new Vector(
  window.innerWidth * 0.5,
  window.innerHeight * 0.1
);
const lightSourceHeight = 1.5 * Math.min(window.innerWidth, window.innerHeight);

function calcEffectPosition(position: Vector, radius: number = ballRadius) {
  const lightDirection = lightsource.subtract(position);

  const norm = Math.sqrt(lightDirection.quad() + lightSourceHeight ** 2);
  lightDirection.x /= norm;
  lightDirection.y /= norm;

  const shinyPosition = new Vector(
    -lightDirection.x * 50,
    lightDirection.y * 50
  );
  const shadowPosition = position.subtract(lightDirection.scale(radius));
  return { shadowPosition, shinyPosition };
}
