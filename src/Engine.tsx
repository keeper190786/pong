import {
  getUnitVector,
  inCircle,
  reflect,
  transferVector,
  Vector,
} from "./Vector";
import { center, paddleCircleCenter, PaddleInfo } from "./Paddle";
import plus from "./plus.png";
import laser from "./laser.png";
import gravUp from "./grav_up.png";
import gravDown from "./grav_down.png";
import shrink from "./shrink.png";
import grow from "./grow.png";
import {
  Ball,
  Phase,
  position,
  PowerUpBase,
  PowerUpType,
  State,
  TimedPopUp,
} from "./model";

export const ballRadius = 20;
export const powerUpRadius = 13;

export const maxVelocity = 70;
export const minVelocity = 10;
export const startVelocity = 3.5;
export const fps = 60;

export const percentageOfHeight = 20;
export const angleAtEdge = 30;
export let paddleWidth = 0;
export const maxPaddleVelocity = 14;

const meanTemperatureIncreasePerStep = 1 / 333;
const ballCollisionDistanceQuad = 4 * ballRadius ** 2;
const initialCountBalls = 1;

export function createInitialState(): State {
  // initial conditions

  let storage = JSON.parse(localStorage.getItem("pong_game_data") ?? "0");
  const gravity = 0;
  const laser = false;

  const paddleHeight = Math.max(
    (percentageOfHeight * window.innerHeight) / 100,
    100
  );
  const paddleDiameter =
    paddleHeight +
    Math.max(
      Math.tan((angleAtEdge / 180) * Math.PI) * 0.5 * paddleHeight,
      40
    ) **
      2 /
      4 /
      paddleHeight;
  const left: PaddleInfo = {
    side: "left",
    top: window.innerWidth * 0.5,
    vy: 0,
    centerX:
      window.innerWidth +
      Math.sqrt(0.25 * paddleDiameter ** 2 - 0.25 * paddleHeight ** 2) -
      paddleWidth,
    score: 0,
    target: window.innerWidth * 0.5,
    paddleHeight,
    paddleDiameter,
  };
  const right: PaddleInfo = {
    side: "right",
    top: window.innerWidth * 0.5,
    vy: 0,
    centerX:
      -Math.sqrt(0.25 * paddleDiameter ** 2 - 0.25 * paddleHeight ** 2) +
      paddleWidth,
    score: 0,
    target: -1,
    paddleHeight,
    paddleDiameter,
  };
  const timedPopUp: TimedPopUp = { timer: 0, content: <></> };
  const ai: [boolean, boolean] = [false, true];

  const speed = storage.speed || minVelocity;

  const balls: Ball[] =
    initBallsFromStorage(storage.balls) || initBalls(initialCountBalls);
  const powerUps: PowerUpBase[] =
    initPowerUpsFromStorage(storage.powerUps) || [];
  if (storage !== 0) {
    right.score = storage.right ?? 0;
    left.score = storage.left ?? 0;
    ai[0] = storage.leftAI ?? ai[0];
    ai[1] = storage.rightAI ?? ai[1];
  }

  return {
    balls,
    powerUps,
    left,
    right,
    ai,
    speed,
    gravity,
    laser,
    timedPopUp,
  };
}

function getPowerUpStartPosition() {
  return {
    ...getRandomPhase(0),
    position: new Vector(window.innerWidth * 0.5, 0),
  };
}

enum PowerUps {
  addBall,
  shrink,
  grow,
  laser,
  increaseGravity,
  decreaseGravity,
}

export function getRandomPowerUp(_gravity: number): PowerUpBase {
  const catagory: PowerUps = Math.floor(6 * Math.random() ** 3);
  switch (catagory) {
    case PowerUps.addBall:
      return getAddBallPowerUp();
    case PowerUps.increaseGravity:
      return getGravityPowerUp("increaseGravity");
    case PowerUps.decreaseGravity:
      return getGravityPowerUp("decreaseGravity");
    case PowerUps.laser:
      return getLaserPowerUp();
    case PowerUps.shrink:
      return getShrinkPowerUp();
    case PowerUps.grow:
      return getGrowPowerUp();
    default:
      throw new Error(
        "Something went wrong. getRandomPowerUp fails. " +
          catagory +
          " " +
          Object.keys(PowerUps).length
      );
  }
}

function getAddBallPowerUp(
  phase: Phase = getPowerUpStartPosition()
): PowerUpBase {
  return {
    type: "addBall",
    position: phase.position,
    velocityDirection: phase.velocityDirection,
    velocityMagnitude: phase.velocityMagnitude,
    cooldown: 0,
    image: plus,
    primaryColor: "blue",
    secundaryColor: "darkblue",
    shadowColor: [0, 0, 0],
    applyEffectOn(ball: Ball, state: State): void {
      ball.velocityMagnitude *= 0.5;
      state.balls.push({
        ...ball,
        velocityDirection: ball.velocityDirection.scale(-1),
        laser: false,
      });
      this.cooldown = 2 * fps; // 2 seconds in milliseconds
    },
  };
}

function getGravityPowerUp(
  type: "increaseGravity" | "decreaseGravity",
  phase: Phase = getPowerUpStartPosition()
): PowerUpBase {
  return {
    type,
    position: phase.position,
    velocityDirection: phase.velocityDirection,
    velocityMagnitude: phase.velocityMagnitude,
    cooldown: 2, // 2 seconds in milliseconds
    image: type === "increaseGravity" ? gravDown : gravUp,
    primaryColor: type === "increaseGravity" ? "green" : '"red"',
    secundaryColor: type === "increaseGravity" ? "darkgreen" : "darkred",
    shadowColor: [0, 0, 0],
    applyEffectOn(ball: Ball, state: State): void {
      if (this.type === "increaseGravity") {
        this.type = "decreaseGravity";
        state.gravity = 100;
      } else if (this.type === "decreaseGravity") {
        this.type = "increaseGravity";
        state.gravity = 0;
      }

      this.image = state.gravity === 0 ? gravDown : gravUp;
      this.primaryColor = state.gravity === 0 ? "green" : "red";
      this.secundaryColor = state.gravity === 0 ? "darkgreen" : "darkred";

      this.cooldown = 2 * fps;
    },
  };
}

function getLaserPowerUp(
  phase: Phase = getPowerUpStartPosition()
): PowerUpBase {
  return {
    type: "laser",
    position: phase.position,
    velocityDirection: phase.velocityDirection,
    velocityMagnitude: phase.velocityMagnitude,
    cooldown: 5, // 2 seconds in milliseconds,
    image: laser,
    primaryColor: "rgba(200, 100,50, 0.8)",
    secundaryColor: "rgba(100, 50,25, 1.0)",
    shadowColor: [0, 0, 0],
    applyEffectOn(ball: Ball, _state: State): void {
      ball.laser = true;

      this.cooldown = 5 * fps;
    },
  };
}

function getShrinkPowerUp(
  phase: Phase = getPowerUpStartPosition()
): PowerUpBase {
  return {
    type: "shrink",
    position: phase.position,
    velocityDirection: phase.velocityDirection,
    velocityMagnitude: phase.velocityMagnitude,
    cooldown: 10, // 2 seconds in milliseconds
    image: shrink,
    primaryColor: "fuchsia",
    secundaryColor: "DarkCyan",
    shadowColor: [0, 0, 0],
    applyEffectOn(ball: Phase, state: State): void {
      if (ball.velocityDirection.x > 0) {
        state.left.centerX += 10;
      } else {
        state.right.centerX -= 10;
      }
      this.cooldown = 10 * fps;
    },
  };
}

function getGrowPowerUp(phase: Phase = getPowerUpStartPosition()): PowerUpBase {
  return {
    type: "grow",
    position: phase.position,
    velocityDirection: phase.velocityDirection,
    velocityMagnitude: phase.velocityMagnitude,
    cooldown: 10, // 2 seconds in milliseconds
    image: grow,
    primaryColor: "DarkCyan",
    secundaryColor: "Fuchsia",
    shadowColor: [0, 0, 0],
    applyEffectOn(ball: Phase, state: State): void {
      if (ball.velocityDirection.x > 0) {
        state.right.centerX += 10;
      } else {
        state.left.centerX -= 10;
      }
      this.cooldown = 10 * fps;
    },
  };
}

export function saveData(state: State) {
  const gameData = {
    left: state.left.score,
    right: state.right.score,
    leftAI: state.ai[0],
    rightAI: state.ai[1],
    balls: state.balls,
    powerUps: state.powerUps,
    speed: state.speed,
  };

  localStorage.setItem("pong_game_data", JSON.stringify(gameData));
  return state;
}

export function addPowerUpsRemoveOld(newState: State) {
  for (let i = newState.powerUps.length - 1; i >= 0; i--) {
    if (newState.powerUps[i].position.y > window.innerHeight * 1.2) {
      newState.powerUps.splice(i, 1);
    }
  }
  if (newState.powerUps.length < 3) {
    newState.powerUps = [
      ...newState.powerUps,
      getRandomPowerUp(newState.gravity),
    ];
  }
}
export function moveBallsAndPowerUps(newState: State) {
  newState.speed = Math.min(
    newState.speed + meanTemperatureIncreasePerStep,
    maxVelocity
  );
  newState.powerUps = propagatePhases(
    newState.powerUps,
    1,
    newState.gravity,
    new Vector(0, 0.01),
    0.02,
    {
      topBottom: false,
      leftRight: true,
    }
  );

  newState.balls = propagatePhases(
    newState.balls,
    newState.speed,
    newState.gravity
  );
  for (let i = 0; i < newState.balls.length; i++) {
    for (const powerUp of newState.powerUps) {
      if (powerUp.cooldown === 0) {
        if (
          inCircle(powerUp.position, newState.balls[i].position, (powerUpRadius+ballRadius))
        ) {
          powerUp.applyEffectOn(newState.balls[i], newState);
        }
      }
    }

    if (
      newState.balls[i].position.x > 0 &&
      newState.balls[i].position.x < window.innerWidth
    ) {
      for (let j = i + 1; j < newState.balls.length; j++) {
        const deltaX = newState.balls[j].position.subtract(
          newState.balls[i].position
        );
        if (deltaX.quad() < ballCollisionDistanceQuad) {
          let newVelocityI = newState.balls[i].velocityDirection.scale(
            newState.balls[i].velocityMagnitude
          );
          let newVelocityJ = newState.balls[j].velocityDirection.scale(
            newState.balls[j].velocityMagnitude
          );
          const projection = deltaX.scalarMultiply(
            newVelocityJ.subtract(newVelocityI)
          );
          if (projection < 0) {
            // balls approach another

            const dX = deltaX.normed();
            const velocityTransferIJ = dX
              .scale(newVelocityI.scalarMultiply(dX))
              .subtract(dX.scale(newVelocityJ.scalarMultiply(dX)));
            transferVector(newVelocityJ, newVelocityI, velocityTransferIJ);
            const [newV, magnitude] = newVelocityI.normedPlusNorm();
            const [newVJ, magnitudeJ] = newVelocityJ.normedPlusNorm();
            newState.balls[i] = {
              ...newState.balls[i],
              velocityDirection: newV,
              velocityMagnitude: magnitude,
            };
            newState.balls[j] = {
              ...newState.balls[j],
              velocityDirection: newVJ,
              velocityMagnitude: magnitudeJ,
            };
          }
        }
      }
    }
  }
  for (const powerUp of newState.powerUps) {
    powerUp.cooldown--;
    if (powerUp.cooldown < 0) {
      powerUp.cooldown = 0;
    }
  }
}

export function collideWithPaddle(newState: State) {
  let pointsLeft = 0;
  let pointsRight = 0;
  const effectivePaddleBallRadius =
    (newState.left.paddleDiameter + 2 * ballRadius) * 0.5;
  for (const ball of newState.balls) {
    if (
      ball.position.x < window.innerWidth - paddleWidth + ballRadius &&
      ball.position.x > paddleWidth - ballRadius
    ) {
      if (
        inCircle(
          ball.position,
          center(newState.left),
          effectivePaddleBallRadius
        )
      ) {
        const [differenceLeft, distance] = ball.position
          .subtract(center(newState.left))
          .normedPlusNorm();
        if (differenceLeft.scalarMultiply(ball.velocityDirection) < 0) {
          ball.velocityDirection = reflect(
            ball.velocityDirection,
            differenceLeft
          );
          if (ball.velocityDirection.x > 0) ball.velocityDirection.x *= -1;
        } else {
          const move = differenceLeft.scale(
            (newState.left.paddleDiameter * 0.5 + ballRadius - distance) *
              1.00001
          );
          ball.position = ball.position.add(move);
          if (ball.velocityDirection.x > 0) ball.velocityDirection.x *= -1;
        }
      }

      if (
        inCircle(
          ball.position,
          center(newState.right),
          effectivePaddleBallRadius
        )
      ) {
        const [differenceRight, distance] = ball.position
          .subtract(center(newState.right))
          .normedPlusNorm();
        if (differenceRight.scalarMultiply(ball.velocityDirection) < 0) {
          ball.velocityDirection = reflect(
            ball.velocityDirection,
            differenceRight
          );
          if (ball.velocityDirection.x < 0) ball.velocityDirection.x *= -1;
        } else {
          const move = differenceRight.scale(
            (newState.left.paddleDiameter * 0.5 + ballRadius - distance) *
              1.00001
          );

          ball.position = ball.position.add(move);
          if (ball.velocityDirection.x < 0) ball.velocityDirection.x *= -1;
        }
      }
    } else {
      ball.position.x < 0 ? pointsLeft++ : pointsRight++;
    }
  }

  newState.timedPopUp = { timer: 0, content: <></> };
  if (
    newState.balls.length - pointsRight - pointsLeft < // balls left
      Math.abs(pointsLeft - pointsRight) || // lead
    newState.balls.length - pointsRight - pointsLeft === 0
  ) {
    resetGame(newState);
    let winner = {
      name: "",
      color: "black",
    };
    if (pointsLeft >= pointsRight) {
      newState.left.score++;
      winner.name = newState.ai[0] ? "Red wins!" : "You win!!!";
      winner.color = "rgb(199,75,62)";
    }
    if (pointsRight >= pointsLeft) {
      if (winner.name.length === 0) {
        winner.name = "Green wins!";
        winner.color = "rgb(62,199,85)";
      } else {
        winner.name = "Draw";
        winner.color = "rgb(199,199,199)";
      }
      newState.right.score++;
    }
    newState.powerUps.forEach((powerUp) => {
      if (powerUp.type === "decreaseGravity") powerUp.type = "increaseGravity";
    });
    newState.gravity = 0;
    newState.speed = minVelocity;
    newState.timedPopUp.timer = 3000;
    newState.timedPopUp.content = (
      <div
        style={{
          marginLeft: "auto",
          marginTop: "auto",
          fontSize: "30pt",
          backgroundColor: `${winner.color}`,
        }}
      >
        {" "}
        {winner.name}
      </div>
    );

    saveData(newState);
  }
}

let count = 0;
export function movePaddle(state: State) {
  state.left.vy = 0;
  state.right.vy = 0;

  if (count++ > 5) {
    count = 0;
    if (state.ai[0] || state.ai[1]) {
      let fastestImpactLeft = Number.MAX_SAFE_INTEGER;
      let fastestImpactRight = Number.MAX_SAFE_INTEGER;
      for (const ball of state.balls) {
        if (
          ball.position.x > -ballRadius &&
          ball.position.x < window.innerWidth + ballRadius
        ) {
          if (state.ai[0] && ball.velocityDirection.x > 0) {
            const distanceLeft = window.innerWidth - ball.position.x;
            if (
              fastestImpactLeft >
              distanceLeft / ball.velocityDirection.x / ball.velocityMagnitude
            ) {
              fastestImpactLeft =
                distanceLeft /
                ball.velocityDirection.x /
                ball.velocityMagnitude;
              // noinspection JSSuspiciousNameCombination
              state.left.target = foldBackToDomain(
                position(ball, fastestImpactLeft, state.gravity).y
                //ball.position.y + fastestImpactLeft * ball.velocityDirection.y* ball.velocityMagnitude
              );
            }
          } else if (state.ai[1] && ball.velocityDirection.x < 0) {
            const distanceRight = ball.position.x;
            if (
              fastestImpactRight >
              -distanceRight / ball.velocityDirection.x / ball.velocityMagnitude
            ) {
              fastestImpactRight =
                -distanceRight /
                ball.velocityDirection.x /
                ball.velocityMagnitude;
              // noinspection JSSuspiciousNameCombination
              state.right.target = foldBackToDomain(
                //ball.position.y + fastestImpactRight * ball.velocityDirection.y
                position(ball, fastestImpactRight, state.gravity).y
              );
            }
          }
        }
      }
    }
  }

  if (state.right.target <= 0 && state.right.target > window.innerHeight) {
    state.right.target = window.innerHeight * 0.5;
  }
  if (state.left.target <= 0 && state.left.target > window.innerHeight) {
    state.left.target = window.innerHeight * 0.5;
  }

  state.right.vy = velocityToReachPosition(
    state.right.top,
    state.right.target,
    state.right.paddleHeight
  );
  state.left.vy = velocityToReachPosition(
    state.left.top,
    state.left.target,
    state.left.paddleHeight
  );

  if (state.ai[0]) state.left.vy *= 0.5;
  if (state.ai[1]) state.right.vy *= 0.5;

  state.right.top += state.right.vy;
  if (state.right.top < 0) state.right.top = 0;
  if (state.right.top > window.innerHeight - state.right.paddleHeight)
    state.right.top = window.innerHeight - state.right.paddleHeight;

  state.left.top += state.left.vy;
  if (state.left.top < 0) state.left.top = 0;
  if (state.left.top > window.innerHeight - state.left.paddleHeight)
    state.left.top = window.innerHeight - state.left.paddleHeight;
}

///////////////////////////////////////////////////
//               private functions               //
///////////////////////////////////////////////////

function reflectPhaseOnWindow(
  phase: Phase,
  sides: Orientations = { leftRight: false, topBottom: true }
) {
  const velocityDirection = new Vector(
    phase.velocityDirection.x,
    phase.velocityDirection.y
  );
  const position = new Vector(phase.position.x, phase.position.y);
  if (sides.topBottom) {
    if (position.y < ballRadius) {
      velocityDirection.y = Math.abs(velocityDirection.y);
      position.y = 2 * ballRadius - position.y;
    }
    if (position.y > window.innerHeight - ballRadius) {
      velocityDirection.y = -Math.abs(velocityDirection.y);
      position.y = 2 * (window.innerHeight - ballRadius) - position.y;
    }
  }

  if (sides.leftRight) {
    if (position.x < ballRadius) {
      velocityDirection.x = Math.abs(velocityDirection.x);
      position.x = 2 * ballRadius - position.x;
    }
    if (position.x > window.innerWidth - ballRadius) {
      velocityDirection.x = -Math.abs(velocityDirection.x);
      position.x = 2 * (window.innerWidth - ballRadius) - position.x;
    }
  }

  return { position, velocityDirection };
}

type Orientations = { topBottom: boolean; leftRight: boolean };
function propagatePhases<T extends Phase>(
  phases: T[],
  speed: number,
  gravity: number,
  drift: Vector = new Vector(0, 0),
  angleNoise: number = 0,
  reflect: Orientations = { topBottom: true, leftRight: false }
): T[] {
  return phases.map((phase) => {
    let velocityMagnitude = Math.max(
      0,
      phase.velocityMagnitude === 0
        ? 0
        : phase.velocityMagnitude +
            (0.1 * (speed - phase.velocityMagnitude)) / 1000
    );
    let { velocityDirection, position } = reflectPhaseOnWindow(phase, reflect);

    const velocity = velocityDirection
      .scale(velocityMagnitude)
      .add(new Vector(drift.x, drift.y + gravity / 2000));

    position = position.add(velocity);

    [velocityDirection, velocityMagnitude] = velocity.normedPlusNorm();

    if (angleNoise > -Math.log(Math.random())) {
      velocityDirection = getUnitVector().add(drift);
      velocityDirection = velocityDirection.normed();
      velocityMagnitude = 1;
    }
    return { ...phase, position, velocityDirection, velocityMagnitude };
  });
}

export function rotateUnitVector(toRotate: Vector, addAngle: number): Vector {
  const angle = Math.atan2(toRotate.y, toRotate.x) + addAngle;
  return new Vector(Math.sin(angle), Math.cos(angle));
}

function resetGame(newState: State) {
  newState.balls.length = initialCountBalls;
  for (let i = 0; i < newState.balls.length; i++) {
    const ball = newState.balls[i];
    ball.velocityDirection.x *= -1;
    ball.position.x =
      window.innerWidth *
      (ball.velocityDirection.x > 0
        ? 0.25 + 0.25 * Math.random()
        : 0.5 + 0.25 * Math.random());
    ball.velocityMagnitude = startVelocity;
  }
  newState.left.centerX = paddleCircleCenter(newState.left);
  newState.right.centerX = paddleCircleCenter(newState.right);
}

function velocityToReachPosition(
  currentTop: number,
  position: number,
  paddleHeight: number
) {
  if (position > currentTop + 0.5 * paddleHeight + maxPaddleVelocity) {
    return maxPaddleVelocity;
  } else if (position < currentTop + 0.5 * paddleHeight - maxPaddleVelocity) {
    return -maxPaddleVelocity;
  } else {
    return 0;
  }
}

export function getRandomPhase(velocityMagnitude: number = minVelocity): Phase {
  const position = new Vector(
    window.innerWidth * (0.25 + 0.5 * Math.random()),
    window.innerHeight * Math.random()
  );
  const velocityDirection = getUnitVector();
  return { position, velocityDirection, velocityMagnitude };
}

function initBalls(count: number): Ball[] {
  const phases: Ball[] = [];
  for (let i = 0; i < count; i++) {
    phases.push({ ...getRandomPhase(startVelocity), laser: true });
  }
  return phases;
}
function reparsePhase(dataPhase: any): Phase {
  const position = new Vector(dataPhase.position.x, dataPhase.position.y);
  const velocityDirection = new Vector(
    dataPhase.velocityDirection.x,
    dataPhase.velocityDirection.y
  );
  return {
    position,
    velocityDirection,
    velocityMagnitude: dataPhase.velocityMagnitude,
  };
}
function initBallsFromStorage(data: any[]): Ball[] | undefined {
  if (data === undefined || data[0] === undefined) return undefined;
  const balls: Ball[] = [];
  for (let i = 0; i < data.length; i++) {
    balls.push({
      ...reparsePhase(data[i]),
      laser: data[i].laser,
    });
  }
  return balls;
}

export function getPowerUp(powerUp: any): PowerUpBase | undefined {
  if (powerUp === undefined) return undefined;
  switch (powerUp.type as PowerUpType) {
    case "addBall":
      return getAddBallPowerUp(reparsePhase(powerUp));
    case "increaseGravity":
      return getGravityPowerUp("increaseGravity", reparsePhase(powerUp));
    case "decreaseGravity":
      return getGravityPowerUp("decreaseGravity", reparsePhase(powerUp));
    case "laser":
      return getLaserPowerUp(reparsePhase(powerUp));
    case "shrink":
      return getShrinkPowerUp(reparsePhase(powerUp));
    case "grow":
      return getGrowPowerUp(reparsePhase(powerUp));
    default:
      throw new Error(
        "Something went wrong. getPowerUp fails. " +
          powerUp.type +
          " " +
          Object.keys(PowerUps).length +
          " \n " +
          JSON.stringify(powerUp, undefined, "\t")
      );
  }
}

function initPowerUpsFromStorage(data: any[]): PowerUpBase[] | undefined {
  if (data === undefined) return undefined;
  const powerUps: PowerUpBase[] = [];
  for (let i = 0; i < data.length; i++) {
    const dummy = getPowerUp(data[i]);
    if (dummy !== undefined) {
      powerUps.push(dummy);
    }
  }
  return powerUps;
}

export function foldBackToDomain(x: number, m: number = window.innerHeight) {
  const r = Math.floor((x - ballRadius) / (m - 2 * ballRadius));
  //console.log(r,x,m,x/m);
  if (Math.abs(r) % 2 === 0) {
    return x - r * m + ballRadius;
  } else {
    return m - (x - r * m) + ballRadius;
  }
}
