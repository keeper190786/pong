import { Reducer } from "react";
import {
  addPowerUpsRemoveOld,
  collideWithPaddle,
  fps,
  moveBallsAndPowerUps,
  movePaddle,
  saveData,
  percentageOfHeight,
  angleAtEdge,
} from "./Engine";
import { State, StateAction } from "./model";
import { paddleCircleCenter } from "./Paddle";

let stepCount = 0;
export const reducer: Reducer<State, StateAction> = (
  state: State,
  action: StateAction
): State => {
  switch (action.type) {
    case "step":
      if (state.timedPopUp.timer === 0) {
        // Consider: move to IMMER
        const newState: State = {
          ...state,
          left: { ...state.left },
          right: { ...state.right },
          ai: [...state.ai],
        };

        movePaddle(newState);
        moveBallsAndPowerUps(newState);
        collideWithPaddle(newState);
        addPowerUpsRemoveOld(newState);

        if (stepCount++ % 50 === 0) {
          saveData(newState);
        }
        return newState;
      } else {
        return {
          ...state,
          timedPopUp: {
            content: { ...state.timedPopUp.content },
            timer:
              state.timedPopUp.timer < 0
                ? 0
                : state.timedPopUp.timer - 1000 / fps,
          },
        };
      }
    case "newPaddleStateLeft":
      const paddleHeight = Math.max(
        (percentageOfHeight * window.innerHeight) / 100,
        100
      );
      const paddleDepth = Math.max(
        Math.tan((angleAtEdge / 180) * Math.PI) * 0.5 * paddleHeight,
        40
      );
      const paddleDiameter = paddleHeight + paddleDepth ** 2 / 4 / paddleHeight;

      return {
        ...state,
        balls: state.balls.map((ball) => ball),
        left: {
          ...state.left,
          ...action.payload!,
          paddleHeight,
          paddleDiameter,
          centerX: paddleCircleCenter(state.left),
        },
        right: {
          ...state.right,
          paddleHeight,
          paddleDiameter,
          centerX: paddleCircleCenter(state.right),
        },
      };
    case "newPaddleStateRight":
      return {
        ...state,
        balls: state.balls.map((ball) => ball),
        right: { ...state.right, ...action.payload! },
      };
    case "toggleAiLeft":
      return saveData({
        ...state,
        ai: [!state.ai[0], state.ai[1]],
        balls: state.balls.map((ball) => ball),
      });
    case "aiLeftOn":
      return saveData({
        ...state,
        ai: [true, state.ai[1]],
      });
    case "aiLeftOff":
      return saveData({
        ...state,
        ai: [false, state.ai[1]],
      });
    case "toggleAiRight":
      return saveData({
        ...state,
        ai: [state.ai[0], !state.ai[1]],
        balls: state.balls.map((ball) => ball),
      });
    case "setGravity":
      return {
        ...state,
        gravity: action.payload,
      };
    default:
      throw new Error("Unknown state change requested: " + action.type);
  }
};
